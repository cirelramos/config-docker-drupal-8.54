<?php


$databases = array();

$config_directories = array();

$settings['hash_salt'] = 'nJ3X6T3t-W3YMV1HgR9ZSBprALFYZ87DD8GcN088vfI_YSZFJK1fNm4pdTFMP0U8AO6yMoKtUw';

$settings['update_free_access'] = FALSE;

$settings['container_yamls'][] = $app_root . '/' . $site_path . '/services.yml';


$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];


$settings['entity_update_batch_size'] = 50;


 if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
   include $app_root . '/' . $site_path . '/settings.local.php';
 }
$databases['default']['default'] = array (
  'database' => getenv('DB_NAME', 'drupal'),
  'username' => getenv('DB_USER', 'drupal'),
  'password' => getenv('DB_PASSWORD', 'drupal'),
  'prefix' => '',
  'host' => getenv('DB_HOST', '172.19.0.2'),
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);
$settings['install_profile'] = 'standard';
$config_directories['sync'] = 'sites/default/files/config_NDbuO7_Xyh1Ybwu_F52VMJwf5mhW2ysmWPQE-uf05haFLpcR8NFtrXYaHD86fBspjzwarxnz2A/sync';
